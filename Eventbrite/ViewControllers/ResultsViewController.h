//
//  ResultsViewController.h
//  Eventbrite
//
//  Created by Christian Roman on 01/04/14.
//  Copyright (c) 2014 Christian Roman. All rights reserved.
//

#import <UIKit/UIKit.h>

@import CoreLocation;

@interface ResultsViewController : UIViewController

@property (nonatomic, strong) NSArray *events;
@property (nonatomic, strong) CLLocation *location;
@property (nonatomic, assign) BOOL includeUserLocation;

@end
