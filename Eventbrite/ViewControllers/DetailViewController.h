//
//  DetailViewController.h
//  Eventbrite
//
//  Created by Christian Roman on 01/04/14.
//  Copyright (c) 2014 Christian Roman. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Event;

@interface DetailViewController : UIViewController

@property (nonatomic, strong) Event *event;

@end
