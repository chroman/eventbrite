//
//  MainViewController.m
//  Eventbrite
//
//  Created by Christian Roman on 28/03/14.
//  Copyright (c) 2014 Christian Roman. All rights reserved.
//

#import "MainViewController.h"
#import "CRClient+Events.h"
#import "ResultsViewController.h"
#import "DETAnimatedTransitioning.h"

@interface MainViewController () <UIViewControllerTransitioningDelegate>

@property (nonatomic, weak) IBOutlet UITextField *textField;

@end

@implementation MainViewController

#pragma mark - UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"EventNow";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods

- (void)searchEvents
{
    NSString *keyword = [_textField text];
    
    [[CRClient sharedClient] getEventsWithKeyword:(NSString *)keyword completion:^(NSArray *events, NSError *error) {
        if (!error) {
            
            ResultsViewController *resultsViewController = [[ResultsViewController alloc] init];
            [resultsViewController setEvents:events];
            resultsViewController.transitioningDelegate = self;
            [self.navigationController pushViewController:resultsViewController animated:YES];
            
        } else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _textField) {
        [textField resignFirstResponder];
        
        if (![textField.text isEqualToString:@""]) {
            [self searchEvents];
        }
        
    }
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.3 animations:^{
        
        CGRect viewFrame = self.view.frame;
        viewFrame.origin.y -= 180;
        self.view.frame = viewFrame;
        
    } completion:nil];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.3 animations:^{
        
        CGRect viewFrame = self.view.frame;
        viewFrame.origin.y += 180;
        self.view.frame = viewFrame;
        
    } completion:nil];
}

#pragma mark - UIViewControllerTransitioningDelegate

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    DETAnimatedTransitioning *transitioning = [DETAnimatedTransitioning new];
    return transitioning;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    DETAnimatedTransitioning *transitioning = [DETAnimatedTransitioning new];
    transitioning.reverse = YES;
    return transitioning;
}

@end
