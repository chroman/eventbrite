//
//  ResultsViewController.m
//  Eventbrite
//
//  Created by Christian Roman on 01/04/14.
//  Copyright (c) 2014 Christian Roman. All rights reserved.
//

#import "ResultsViewController.h"
#import "EventAnnotation.h"
#import "Event.h"
#import "Organizer.h"
#import "Venue.h"
#import "CRClient+Events.h"
#import "DetailViewController.h"
#import "EventCell.h"
#import "WilcardGestureRecognizer.h"
#import "UIColor+Utilities.h"

@import MapKit;
@import CoreLocation;
@import CoreGraphics;

#define METERS_TO_FEET  3.2808399
#define METERS_TO_MILES 0.000621371192
#define METERS_CUTOFF   1000
#define FEET_CUTOFF     3281
#define FEET_IN_MILES   5280

@interface ResultsViewController () <MKMapViewDelegate, CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *userLocation;

@property (nonatomic, assign) BOOL mapViewIsOpen;

@property (nonatomic, assign) CGRect mapViewFrame;
@property (nonatomic, assign) CGRect resultsTableViewFrame;

@property (nonatomic, weak) UIButton *userLocationButton;

@property (nonatomic, weak) IBOutlet MKMapView *mapView;
@property (nonatomic, weak) IBOutlet UITableView *resultsTableView;

@property (nonatomic, strong) WildcardGestureRecognizer *_tapInterceptor;

@end

@implementation ResultsViewController

#pragma mark - UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.userLocation = [[CLLocation alloc] init];
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.distanceFilter = kCLDistanceFilterNone;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [self.locationManager startUpdatingLocation];
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Results", nil);
    self.mapViewIsOpen = NO;
    
    self.mapView.frame = CGRectMake(self.mapView.frame.origin.x, self.mapView.frame.origin.y, self.mapView.frame.size.width, 115);
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(self.location.coordinate.latitude, self.location.coordinate.longitude), 2000, 2000);
	[self.mapView setRegion:region animated:NO];
    
    for (Event *event in self.events) {
        
        EventAnnotation *annotation = [[EventAnnotation alloc] init];
        [annotation setTitle:event.title];
        [annotation setSubtitle:event.venue.address];
        [annotation setIndex:[self.events indexOfObject:event]];
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(event.venue.latitude, event.venue.longitude);
        [annotation setCoordinate:coordinate];
        [self.mapView addAnnotation:annotation];
        
    }
    
    self._tapInterceptor = [[WildcardGestureRecognizer alloc] init];
    __weak WildcardGestureRecognizer *tapInterceptor = self._tapInterceptor;
    __weak typeof(self) weakSelf = self;
    tapInterceptor.touchesBeganCallback = ^(NSSet * touches, UIEvent * event) {
        typeof(self) strongSelf = weakSelf;
        [strongSelf.mapView removeGestureRecognizer:tapInterceptor];
        strongSelf._tapInterceptor = nil;
        [strongSelf openMapView];
    };
    [self.mapView addGestureRecognizer:tapInterceptor];
    
    if(![self.events count]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Empty results", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alertView show];
    }
    
    [self zoomMapViewToFitAnnotationsWithUserLocation:self.includeUserLocation];
}

#pragma mark - Class methods

- (void)openMapView
{
    [UIView animateWithDuration:0.5f
                     animations:^{
                         
                         self.mapViewFrame = self.mapView.frame;
                         self.resultsTableViewFrame = self.resultsTableView.frame;
                         [self.mapView setFrame:CGRectMake(self.mapView.frame.origin.x, self.mapView.frame.origin.y, self.mapView.frame.size.width, self.view.frame.size.height - self.mapView.frame.origin.x)];
                         [self.resultsTableView setFrame:CGRectMake(self.resultsTableView.frame.origin.x, self.view.frame.size.height, self.resultsTableView.frame.size.width, self.resultsTableView.frame.size.height)];
                         
                     } completion:^(BOOL finished) {
                         
                         self.userLocationButton = [UIButton buttonWithType:UIButtonTypeCustom];
                         [self.userLocationButton addTarget:self action:@selector(centerMapUserLocation) forControlEvents:UIControlEventTouchUpInside];
                         [self.userLocationButton setFrame:CGRectMake( self.mapView.frame.origin.x + 10, self.mapView.frame.origin.y + 10, 32, 32)];
                         [self.userLocationButton setBackgroundImage:[UIImage imageNamed:@"User Location"] forState:UIControlStateNormal];
                         [self.userLocationButton setAlpha:0.0];
                         [self.mapView addSubview:self.userLocationButton];
                         
                         UIBarButtonItem *close = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStyleDone target:self action:@selector(closeMapView)];
                         [close setTintColor:[UIColor blackColor]];
                         
                         [self.navigationItem setLeftBarButtonItem:close animated:YES];
                         
                         [UIView animateWithDuration:0.5f animations:^{
                             
                             [self.userLocationButton setAlpha:0.8];
                             [self zoomMapViewToFitAnnotationsWithUserLocation:self.includeUserLocation];
                             
                         } completion:^(BOOL finished) {
                             
                             [self.mapView setZoomEnabled:YES];
                             [self.mapView setScrollEnabled:YES];
                             self.mapViewIsOpen = YES;
                             
                         }];
                         
                     }];
}

- (void)closeMapView
{
    [self closeMapViewWithCompletion:nil];
}

- (void)closeMapViewWithCompletion:(void (^)(void))completion
{
    [self.mapView setZoomEnabled:NO];
    [self.mapView setScrollEnabled:NO];
    
    [self.navigationItem setRightBarButtonItems:nil animated:YES];
    [self.navigationItem setLeftBarButtonItem:nil animated:NO];
    
    for (NSObject<MKAnnotation> *annotation in [self.mapView selectedAnnotations])
        [self.mapView deselectAnnotation:(id <MKAnnotation>)annotation animated:NO];
    
    [UIView animateWithDuration:0.5f
                     animations:^{
                         
                         self.userLocationButton.alpha = 0;
                         
                         self.mapView.frame = self.mapViewFrame;
                         self.resultsTableView.frame = self.resultsTableViewFrame;
                         
                     } completion:^(BOOL finished){
                         
                         [self zoomMapViewToFitAnnotationsWithUserLocation:NO];
                         
                         [self.userLocationButton removeFromSuperview];
                         self.userLocationButton = nil;
                         
                         self.mapViewIsOpen = NO;
                         
                         self._tapInterceptor = [[WildcardGestureRecognizer alloc] init];
                         __weak WildcardGestureRecognizer *tapInterceptor = self._tapInterceptor;
                         __weak typeof(self) weakSelf = self;
                         tapInterceptor.touchesBeganCallback = ^(NSSet * touches, UIEvent * event) {
                             typeof(self) strongSelf = weakSelf;
                             [strongSelf.mapView removeGestureRecognizer:tapInterceptor];
                             strongSelf._tapInterceptor = nil;
                             [strongSelf openMapView];
                         };
                         [self.mapView addGestureRecognizer:tapInterceptor];
                         
                         if (completion) {
                             completion();
                         }
                         
                     }];
}

- (void)centerMapUserLocation
{
    [self.mapView setCenterCoordinate:self.userLocation.coordinate animated:YES];
}

- (void)zoomMapViewToFitAnnotationsWithUserLocation:(BOOL)fitToUserLocation
{
    if([self.mapView.annotations count] > 1) {
        MKMapRect zoomRect = MKMapRectNull;
        for (id <MKAnnotation> annotation in self.mapView.annotations) {
            if(fitToUserLocation) {
                MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
                MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.2, 0.2);
                if (MKMapRectIsNull(zoomRect)) {
                    zoomRect = pointRect;
                } else {
                    zoomRect = MKMapRectUnion(zoomRect, pointRect);
                }
            } else {
                if (![annotation isKindOfClass:[MKUserLocation class]] ) {
                    MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
                    MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.2, 0.2);
                    if (MKMapRectIsNull(zoomRect)) {
                        zoomRect = pointRect;
                    } else {
                        zoomRect = MKMapRectUnion(zoomRect, pointRect);
                    }
                }
            }
        }
        [self.mapView setVisibleMapRect:zoomRect animated:YES];
    }
}

- (NSString *)convertDistanceToString:(double)distance
{
    BOOL isMetric = [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue];
    
    NSString *format;
    
    if (isMetric) {
        if (distance < METERS_CUTOFF) {
            format = @"%@ m";
        } else {
            format = @"%@ km";
            distance = distance / 1000;
        }
    } else {
        distance = distance * METERS_TO_FEET;
        if (distance < FEET_CUTOFF) {
            format = @"%@ ft";
        } else {
            format = @"%@ mi";
            distance = distance / FEET_IN_MILES;
        }
    }
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setLocale:[NSLocale currentLocale]];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:1];
    return [NSString stringWithFormat:format, [numberFormatter stringFromNumber:[NSNumber numberWithDouble:distance]]];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.events count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    EventCell *cell = (EventCell *) [self.resultsTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EventCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    Event *event = [self.events objectAtIndex:indexPath.row];
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:event.venue.latitude longitude:event.venue.longitude];
    double distance = [location distanceFromLocation:self.userLocation];
    
    [cell.name setText:event.title];
    [cell.address setText:event.venue.address];
    [cell.distance setText:[self convertDistanceToString:distance]];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Event *event = [self.events objectAtIndex:indexPath.row];
    DetailViewController *detailViewController = [[DetailViewController alloc] init];
    [detailViewController setEvent:event];
    [self.navigationController pushViewController:detailViewController animated:YES];
    [self.resultsTableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    static NSString *AnnotationViewID = @"annotationViewID";
    
    MKPinAnnotationView *annotationView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
    if (!annotationView) {
        
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
        
        annotationView.pinColor = MKPinAnnotationColorRed;
        
        annotationView.canShowCallout = YES;
        annotationView.draggable = NO;
        
    } else {
        annotationView.annotation = annotation;
    }
    
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    return annotationView;
}

- (void)mapView:(MKMapView *)mv annotationView:(MKAnnotationView *)pin calloutAccessoryControlTapped:(UIControl *)control
{
    __weak typeof(self) weakSelf = self;
    [self closeMapViewWithCompletion:^{
        
        typeof(self) strongSelf = weakSelf;
        EventAnnotation *annotation = (EventAnnotation *)pin.annotation;
        Event *event = [self.events objectAtIndex:annotation.index];
        DetailViewController *detailViewController = [[DetailViewController alloc] init];
        [detailViewController setEvent:event];
        [strongSelf.navigationController pushViewController:detailViewController animated:YES];
        
    }];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    self.userLocation = [locations lastObject];
    [self.resultsTableView reloadData];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:[error localizedDescription] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
    [alertView show];
}

@end
