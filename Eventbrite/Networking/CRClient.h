//
//  CRClient.h
//  Eventbrite
//
//  Created by Christian Roman on 31/03/14.
//  Copyright (c) 2014 Christian Roman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CRClient : NSObject

@property (nonatomic, strong) NSURL *baseURL;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSURLSession *session;

+ (instancetype)sharedClient;

@end
