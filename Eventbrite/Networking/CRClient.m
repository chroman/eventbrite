//
//  CRClient.m
//  Eventbrite
//
//  Created by Christian Roman on 31/03/14.
//  Copyright (c) 2014 Christian Roman. All rights reserved.
//

#import "CRClient.h"

@interface CRClient ()

@property (nonatomic, strong) NSString *userAgent;

+ (NSURL *)APIBaseURL;

@end

@implementation CRClient

static NSString * const kCRClientAPIBaseURLString = @"https://www.eventbrite.com/json/event_search";

static NSString * const kCRClientAPIToken = @"Z2OQTF2BQEAKT6Q4W6";

#pragma mark - Class Methods

+ (instancetype)sharedClient
{
    static dispatch_once_t onceQueue;
    static CRClient *__sharedClient = nil;
    dispatch_once(&onceQueue, ^{
        __sharedClient = [[self alloc] init];
    });
    return __sharedClient;
}

- (instancetype)init
{
    if (self = [super init]) {
        self.baseURL = [[self class] APIBaseURL];
        self.token = [[self class] APIToken];
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        config.URLCache = [[NSURLCache alloc] initWithMemoryCapacity:15 * 1024 * 1024
                                                        diskCapacity:100 * 1024 * 1024
                                                            diskPath:nil];
        _session = [NSURLSession sessionWithConfiguration:config];
    }
    return self;
}

+ (NSURL *)APIBaseURL
{
    return [NSURL URLWithString:kCRClientAPIBaseURLString];
}

+ (NSString *)APIToken
{
    return kCRClientAPIToken;
}

@end