//
//  CRClient+Stations.m
//  Ecobici
//
//  Created by Christian Roman on 25/01/14.
//  Copyright (c) 2014 Christian Roman. All rights reserved.
//

#import "CRClient+Events.h"
#import "Event.h"
#import "Organizer.h"
#import "Venue.h"

@implementation CRClient (Events)

- (NSArray *)parseJSONEvents:(NSDictionary *)json
{
    NSArray *items = [json objectForKey:@"events"];
    
    NSMutableArray *events = [[NSMutableArray alloc] initWithCapacity:items.count];
    for (NSDictionary *item in items) {
        
        NSDictionary *dictEvent = [item objectForKey:@"event"];
        
        if (dictEvent) {
            
            Event *event = [Event new];
            
            NSString *logoUrl = [dictEvent objectForKey:@"logo_url"];
            event.logoUrl = [logoUrl isKindOfClass:[NSNull class]] ? nil : logoUrl;
            
            NSString *timezone = [dictEvent objectForKey:@"timezone"];
            event.timezone = [timezone isKindOfClass:[NSNull class]] ? nil : timezone;
            
            // Organizer
            NSDictionary *organizerDict = [dictEvent objectForKey:@"organizer"];
            Organizer *organizer = [Organizer new];
            
            NSString *urlOrganizer = [organizerDict objectForKey:@"timezone"];
            organizer.url = [urlOrganizer isKindOfClass:[NSNull class]] ? nil : urlOrganizer;
            
            NSString *descriptionOrganizer = [organizerDict objectForKey:@"description"];
            organizer.description = [descriptionOrganizer isKindOfClass:[NSNull class]] ? nil : descriptionOrganizer;
            
            NSString *longDescriptionOrganizer = [organizerDict objectForKey:@"long_description"];
            organizer.longDescription = [longDescriptionOrganizer isKindOfClass:[NSNull class]] ? nil : longDescriptionOrganizer;
            
            NSString *IDOrganizer = [organizerDict objectForKey:@"id"];
            organizer.ID = [IDOrganizer isKindOfClass:[NSNull class]] ? nil : IDOrganizer;
            
            NSString *nameOrganizer = [organizerDict objectForKey:@"name"];
            organizer.name = [nameOrganizer isKindOfClass:[NSNull class]] ? nil : nameOrganizer;
            
            event.organizer = organizer;
            
            NSString *ID = [dictEvent objectForKey:@"id"];
            event.ID = [ID isKindOfClass:[NSNull class]] ? nil : ID;
            
            NSString *category = [dictEvent objectForKey:@"category"];
            event.category = [category isKindOfClass:[NSNull class]] ? nil : category;
            
            NSString *capacity = [dictEvent objectForKey:@"capacity"];
            event.capacity = [capacity isKindOfClass:[NSNull class]] ? 0 : [capacity integerValue];
            
            NSString *title = [dictEvent objectForKey:@"title"];
            event.title = [title isKindOfClass:[NSNull class]] ? nil : title;
            
            NSString *startDate = [dictEvent objectForKey:@"startDate"];
            event.startDate = [startDate isKindOfClass:[NSNull class]] ? nil : startDate;
            
            NSString *status = [dictEvent objectForKey:@"status"];
            event.status = [status isKindOfClass:[NSNull class]] ? nil : status;
            
            NSString *description = [dictEvent objectForKey:@"description"];
            event.description = [description isKindOfClass:[NSNull class]] ? nil : description;
            
            NSString *endDate = [dictEvent objectForKey:@"endDate"];
            event.endDate = [endDate isKindOfClass:[NSNull class]] ? nil : endDate;
            
            NSString *tags = [dictEvent objectForKey:@"tags"];
            event.tags = [tags isKindOfClass:[NSNull class]] ? nil : tags;
            
            NSString *distance = [dictEvent objectForKey:@"distance"];
            event.distance = [distance isKindOfClass:[NSNull class]] ? nil : distance;
            
            NSString *created = [dictEvent objectForKey:@"created"];
            event.created = [created isKindOfClass:[NSNull class]] ? nil : created;
            
            NSString *url = [dictEvent objectForKey:@"url"];
            event.url = [url isKindOfClass:[NSNull class]] ? nil : url;
            
            NSString *privacy = [dictEvent objectForKey:@"privacy"];
            event.privacy = [privacy isKindOfClass:[NSNull class]] ? nil : privacy;
            
            // Venue
            NSDictionary *venueDict = [dictEvent objectForKey:@"venue"];
            Venue *venue = [Venue new];
            
            NSString *cityVenue = [venueDict objectForKey:@"city"];
            venue.city = [cityVenue isKindOfClass:[NSNull class]] ? nil : cityVenue;
            
            NSString *nameVenue = [venueDict objectForKey:@"name"];
            venue.name = [nameVenue isKindOfClass:[NSNull class]] ? nil : nameVenue;
            
            NSString *countryVenue = [venueDict objectForKey:@"country"];
            venue.country = [countryVenue isKindOfClass:[NSNull class]] ? nil : countryVenue;
            
            NSString *regionVenue = [venueDict objectForKey:@"region"];
            venue.region = [regionVenue isKindOfClass:[NSNull class]] ? nil : regionVenue;
            
            NSString *longitudeVenue = [venueDict objectForKey:@"longitude"];
            venue.longitude = [longitudeVenue isKindOfClass:[NSNull class]] ? 0 : [longitudeVenue doubleValue];
            
            NSString *latitudeVenue = [venueDict objectForKey:@"latitude"];
            venue.latitude = [latitudeVenue isKindOfClass:[NSNull class]] ? 0 : [latitudeVenue doubleValue];
            
            NSString *postalCodeVenue = [venueDict objectForKey:@"postal_code"];
            venue.postalCode = [postalCodeVenue isKindOfClass:[NSNull class]] ? nil : postalCodeVenue;
            
            NSString *addressVenue = [venueDict objectForKey:@"address"];
            venue.address = [addressVenue isKindOfClass:[NSNull class]] ? nil : addressVenue;
            
            NSString *countryCodeVenue = [venueDict objectForKey:@"country_code"];
            venue.countryCode = [countryCodeVenue isKindOfClass:[NSNull class]] ? nil : countryCodeVenue;
            
            NSString *IDVenue = [venueDict objectForKey:@"id"];
            venue.ID = [IDVenue isKindOfClass:[NSNull class]] ? nil : IDVenue;
            
            event.venue = venue;
            
            NSString *modified = [dictEvent objectForKey:@"modified"];
            event.modified = [modified isKindOfClass:[NSNull class]] ? nil : modified;
            
            NSString *repeats = [dictEvent objectForKey:@"repeats"];
            event.repeats = [repeats isKindOfClass:[NSNull class]] ? nil : repeats;
            
            [events addObject:event];
            
        }
        
    }
    return events;
}

- (NSURLSessionDataTask *)getEventsWithKeyword:(NSString *)keyword completion:(CRArrayCompletionBlock)completion
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?app_key=%@&keywords=%@", self.baseURL, self.token, keyword]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSURLSessionDataTask *dataTask = [self.session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if (!completion) {
            return;
        }
        
        if (data) {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                id collection = nil;
                NSError *jsonError = nil;
                id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
                
                if (!jsonError) {
                    collection = [self parseJSONEvents:json];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(collection, nil);
                });
                
            });
            
        } else {
            completion(nil, error);
        }
        
    }];
    
    [dataTask resume];
    return dataTask;
}

@end
