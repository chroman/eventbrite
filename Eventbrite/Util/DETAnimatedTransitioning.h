//
//  DETAnimatedTransitioning.h
//  Eventbrite
//
//  Created by Christian Roman on 01/04/14.
//  Copyright (c) 2014 Christian Roman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DETAnimatedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>

@property (nonatomic) BOOL reverse;

@end