//
//  TransitionManager.h
//  Eventbrite
//
//  Created by Christian Roman on 01/04/14.
//  Copyright (c) 2014 Christian Roman. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, TransitionStep){
    INITIAL = 0,
    MODAL
};

@interface TransitionManager : NSObject <UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign) TransitionStep transitionTo;

@end
