//
//  TransitionManager.m
//  Eventbrite
//
//  Created by Christian Roman on 01/04/14.
//  Copyright (c) 2014 Christian Roman. All rights reserved.
//

#import "TransitionManager.h"

@implementation TransitionManager

#pragma mark - UIViewControllerAnimatedTransitioning

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return 1.0;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    CGRect sourceRect = [transitionContext initialFrameForViewController:fromVC];
    
    if(self.transitionTo == MODAL){
        
        CGAffineTransform rotation;
        rotation = CGAffineTransformMakeRotation(M_PI);
        fromVC.view.frame = sourceRect;
        fromVC.view.layer.anchorPoint = CGPointMake(0.5, 0.0);
        fromVC.view.layer.position = CGPointMake(160.0, 0);
        
        UIView *container = [transitionContext containerView];
        [container insertSubview:toVC.view belowSubview:fromVC.view];
        CGPoint final_toVC_Center = toVC.view.center;
        
        toVC.view.center = CGPointMake(-sourceRect.size.width, sourceRect.size.height);
        toVC.view.transform = CGAffineTransformMakeRotation(M_PI/2);
        
        [UIView animateWithDuration:1.0
                              delay:0.0
             usingSpringWithDamping:.8
              initialSpringVelocity:6.0
                            options:UIViewAnimationOptionCurveEaseIn
         
                         animations:^{
                             fromVC.view.transform = rotation;
                             toVC.view.center = final_toVC_Center;
                             toVC.view.transform = CGAffineTransformMakeRotation(0);
                         } completion:^(BOOL finished) {
                             
                             [transitionContext completeTransition:YES];
                             
                         }];
    } else {
        
        CGAffineTransform rotation;
        rotation = CGAffineTransformMakeRotation(M_PI);
        UIView *container = [transitionContext containerView];
        fromVC.view.frame = sourceRect;
        fromVC.view.layer.anchorPoint = CGPointMake(0.5, 0.0);
        fromVC.view.layer.position = CGPointMake(160.0, 0);
        
        [container insertSubview:toVC.view belowSubview:fromVC.view];
        
        toVC.view.layer.anchorPoint = CGPointMake(0.5, 0.0);
        toVC.view.layer.position = CGPointMake(160.0, 0);
        toVC.view.transform = CGAffineTransformMakeRotation(-M_PI);
        
        [UIView animateWithDuration:1.0
                              delay:0.0
             usingSpringWithDamping:0.8
              initialSpringVelocity:6.0
                            options:UIViewAnimationOptionCurveEaseIn
         
                         animations:^{
                             fromVC.view.center = CGPointMake(fromVC.view.center.x - 320, fromVC.view.center.y);
                             toVC.view.transform = CGAffineTransformMakeRotation(-0);
                             
                         } completion:^(BOOL finished) {
                             
                             [transitionContext completeTransition:YES];
                             
                         }];
    }
}

@end