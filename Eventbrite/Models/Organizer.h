//
//  Organizer.h
//  Eventbrite
//
//  Created by Christian Roman on 31/03/14.
//  Copyright (c) 2014 Christian Roman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Organizer : NSObject

@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *description;
@property (nonatomic, copy) NSString *longDescription;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *name;

@end
