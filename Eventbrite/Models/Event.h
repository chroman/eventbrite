//
//  Event.h
//  Eventbrite
//
//  Created by Christian Roman on 31/03/14.
//  Copyright (c) 2014 Christian Roman. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Organizer;
@class Venue;

@interface Event : NSObject

@property (nonatomic, copy) NSString *logoUrl;
@property (nonatomic, copy) NSString *timezone;
@property (nonatomic, strong) Organizer *organizer;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *category;
@property (nonatomic, assign) NSInteger capacity;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *startDate;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *description;
@property (nonatomic, copy) NSString *endDate;
@property (nonatomic, copy) NSString *tags;
@property (nonatomic, copy) NSString *distance;
@property (nonatomic, copy) NSString *created;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *privacy;
@property (nonatomic, strong) Venue *venue;
@property (nonatomic, copy) NSString *modified;
@property (nonatomic, copy) NSString *repeats;

@end
