//
//  Venue.h
//  Eventbrite
//
//  Created by Christian Roman on 31/03/14.
//  Copyright (c) 2014 Christian Roman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Venue : NSObject

@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *region;
@property (nonatomic, assign) double longitude;
@property (nonatomic, assign) double latitude;
@property (nonatomic, copy) NSString *postalCode;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *countryCode;
@property (nonatomic, copy) NSString *ID;

@end
